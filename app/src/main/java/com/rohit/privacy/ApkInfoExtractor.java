package com.rohit.privacy;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ApkInfoExtractor {
    private int mStatusCode;
    Context context1;
  //  String test="";

    public ApkInfoExtractor(Context context2){

        context1 = context2;
    }

    public ApkInfoExtractor() {

    }

    public List<String> GetAllInstalledApkInfo(){

        List<String> ApkPackageName = new ArrayList<>();

        Intent intent = new Intent(Intent.ACTION_MAIN,null);

        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED );

        List<ResolveInfo> resolveInfoList = context1.getPackageManager().queryIntentActivities(intent,0);

        for(ResolveInfo resolveInfo : resolveInfoList){

            ActivityInfo activityInfo = resolveInfo.activityInfo;

            if(!isSystemPackage(resolveInfo)){

                ApkPackageName.add(activityInfo.applicationInfo.packageName);
            }
        }
        return ApkPackageName;

    }

    public boolean isSystemPackage(ResolveInfo resolveInfo){

        return ((resolveInfo.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
    }

    public Drawable getAppIconByPackageName(String ApkTempPackageName){


        Drawable drawable;

        try{
            drawable = context1.getPackageManager().getApplicationIcon(ApkTempPackageName);
        }
        catch (PackageManager.NameNotFoundException e){

            e.printStackTrace();

            drawable = ContextCompat.getDrawable(context1, R.mipmap.ic_launcher);
        }
        return drawable;
    }


    public String GetAppName(String ApkPackageName){

        String Name = "";

        ApplicationInfo applicationInfo;

        PackageManager packageManager = context1.getPackageManager();

        try {

            applicationInfo = packageManager.getApplicationInfo(ApkPackageName, 0);

            if(applicationInfo!=null){

                Name = (String)packageManager.getApplicationLabel(applicationInfo);
            }

        }catch (PackageManager.NameNotFoundException e) {

            e.printStackTrace();
        }
        return Name;
    }
//    public void post(final String urlJsonObj) {
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,urlJsonObj+"/.json", null, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                //Log.d("aaaa", response.toString());
//                if(mStatusCode>=200 && mStatusCode<300)
//                {
//                    Log.d("urlk"+urlJsonObj+"/.json?shallow=true"," "+mStatusCode);
//                }
//                Toast.makeText(context1, "StatusCode: "+mStatusCode, Toast.LENGTH_LONG).show();
//                submit("true");
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                submit("false");
//                Log.d("status of "+urlJsonObj," secure");
//                VolleyLog.d( "Error: " + error.getMessage());
//            }
//        }) {
//
//            @Override
//            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
//                mStatusCode = response.statusCode;
//                return super.parseNetworkResponse(response);
//            }
//        };
//
//        RequestQueue req = Volley.newRequestQueue(context1);
//        req.add(jsonObjReq);
//    }
//
//
//
//    public void checkVulnb(String app_name){
//        Resources res = null;
//        try {
//
//            //I want to use the clear_activities string in Package com.android.settings
//            res = context1.getPackageManager().getResourcesForApplication(app_name);
//            int resourceId = res.getIdentifier(app_name+":string/firebase_database_url", null, null);
//            if(0 != resourceId) {
//                CharSequence s = context1.getPackageManager().getText(app_name, resourceId, null);
//                Log.d("url", "resource = " + s);
//                //   MainActivity obj=new MainActivity();
//                //  obj.post((String) s);
//                post((String) s);
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//
//    }

}
//TODO remove all log in final build